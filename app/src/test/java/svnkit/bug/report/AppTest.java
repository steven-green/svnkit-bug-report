package svnkit.bug.report;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;

import java.nio.file.Path;

class AppTest {

    Path workingCopy;
    SVNClientManager svnClientManager;
    SVNURL repositorySvnUrl;

    @TempDir
    Path tempDirectory;

    @BeforeAll
    static void setUpClass() {
        SVNRepositoryFactoryImpl.setup();
    }

    @BeforeEach
    void setUp() throws Exception {

        svnClientManager = SVNClientManager.newInstance();
        repositorySvnUrl = SVNRepositoryFactory.createLocalRepository(
                tempDirectory.resolve("repository").toFile(), true, false
        );
        workingCopy = tempDirectory.resolve("working-copy");
    }

    @Test
    void doCleanup() throws Exception {

        svnClientManager.getUpdateClient().doCheckout(
                repositorySvnUrl,
                workingCopy.toFile(),
                SVNRevision.HEAD,
                SVNRevision.HEAD,
                SVNDepth.INFINITY,
                false
        );

        svnClientManager.getWCClient().doCleanup(workingCopy.toFile(), true, true, true, true, true, true);
    }

}
